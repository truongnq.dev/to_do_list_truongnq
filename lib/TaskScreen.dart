import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'models/task.dart';
import 'DetailTaskScreen.dart';
class TaskScreen extends StatefulWidget {
  late final int todoId;

  //constr
  TaskScreen({required this.todoId}) : super();
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _TaskScreenState();
  }
}

class _TaskScreenState extends State<TaskScreen>{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text("List of tasks"),
        actions: [
          IconButton(onPressed: (){

          }, icon: Icon(Icons.add),)
        ],  
      ),
      body: FutureBuilder<List<Task>>(future: fetchTasks(widget.todoId), 
      builder: (context, snapshot) {
        if(snapshot.hasError) print(snapshot.error);
        return snapshot.hasData ? TaskList(tasks: snapshot.data) : Center(child: CircularProgressIndicator(),);
      } ,),
    );
  }
}

class TaskList extends StatelessWidget {
  final List<Task>? tasks;
  //constructor
  TaskList({Key? key, required this.tasks}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(itemBuilder: (context, index) {
      return GestureDetector(
        child: Container(
          padding: EdgeInsets.all(10.0),
          color: index % 2 == 0 ? Colors.deepOrangeAccent : Colors.amber,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              new Text(tasks![index].name, style: TextStyle(fontSize: 16.0), ),
              new Text('Finished: ${tasks![index].isfinished}', style: TextStyle(fontSize: 16.0)),
            ],),
        ),
        onTap: () {
          int selectedId = tasks![index].id;
          Navigator.push(context, 
            MaterialPageRoute(builder: (context) => new DetailTaskScreen(task: tasks![index]))
          );
        }, 
      );
    },
      itemCount: tasks!.length,
    );
  }
}