import 'package:app/TaskScreen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
import 'package:app/models/todo.dart';

class TodoScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return TodoScreenState();
  }
}

class TodoScreenState extends State<TodoScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Fetch Todos from API"), ),
      body: FutureBuilder<List<Todo>>(
        future: fetchTodos(),
        builder: (context, snapshot) {
          if(snapshot.hasError) {
            print(snapshot.error);
          }
          if(snapshot.hasData) {
            return TodoList(todos: snapshot.data);
          }
          return Center(
                  child: CircularProgressIndicator(),
                );
        },
      )
    );
  }
}

class TodoList extends StatelessWidget {
  final List<Todo>? todos;

  //constructor
  TodoList({Key? key, required this.todos}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(itemBuilder: (context, index) {
      return GestureDetector(
        child: Container(
          padding: EdgeInsets.all(10.0),
          color: index % 2 == 0 ? Colors.greenAccent : Colors.cyan,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              new Text(todos![index].name, style: TextStyle(fontSize: 16.0), ),
              new Text('Date: ${todos![index].duedate}', style: TextStyle(fontSize: 16.0)),
            ],),
        ),
        onTap: () {
          Navigator.push(context, 
            MaterialPageRoute(builder: (context) => TaskScreen(todoId: todos![index].id))
          );
        }, 
      );
    },
      itemCount: todos!.length,
    );
  }
}
