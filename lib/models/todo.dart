import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;

class Todo {
  late int id;
  late String name;
  late String duedate;
  late String description;

  //Constructor
  Todo(
      {required this.id,
      required this.name,
      required this.duedate,
      required this.description});

  //static method
  factory Todo.fromJson(Map<String, dynamic> json) {
    return Todo(
      id: json['id'],
      name: json['name'],
      duedate: json['duedate'],
      description: json['description'],
    );
  }
}

Future<List<Todo>> fetchTodos() async {
  var resp;
  try {
    resp = await http.get(Uri.parse('http://localhost:3000/todos'));
  } catch (e) {
    print(e);
  }
  List<dynamic> mapResponse = json.decode(resp.body);
  if (resp.statusCode == 200) {
    final listToBlog = mapResponse.map((json) {
      return Todo.fromJson(json);
    }).toList();
    return listToBlog;
  } else {
    throw Exception("Failed to load Todo from the Internet");
  }
}
