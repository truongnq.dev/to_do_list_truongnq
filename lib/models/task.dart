import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;

class Task {
  late int id;
  late String name;
  late bool isfinished;
  late int todosId;

  //Constructor
  Task(
      {required this.id,
      required this.name,
      required this.isfinished,
      required this.todosId});

  //static method
  factory Task.fromJson(Map<String, dynamic> json) {
    return Task(
      id: json['id'],
      name: json['name'],
      isfinished: json['isfinished'],
      todosId: json['todosId'],
    );
  }
}

Future<List<Task>> fetchTasks(int todosId) async {
  var resp;
  try {
    resp = await http.get(Uri.parse('http://localhost:3000/tasks?todosId_like=${todosId}'));
  } catch (e) {
    print(e);
  }
  List<dynamic> mapResponse = json.decode(resp.body);
  if (resp.statusCode == 200) {
    final tasks = mapResponse.map((json) {
      return Task.fromJson(json);
    }).toList();
    return tasks;
  } else {
    throw Exception("Failed to load Task from the Internet");
  }
}
