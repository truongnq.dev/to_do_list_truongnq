import 'package:flutter/material.dart';
import 'package:app/TodoScreen.dart';
void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Todolist',
      home: new TodoScreen(),
    );
  }
}
