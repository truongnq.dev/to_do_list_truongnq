import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'models/task.dart';

class DetailTaskScreen extends StatefulWidget {
  final Task task;
  //constr
  DetailTaskScreen({required this.task}) : super();
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _DetailTaskScreenState();
  }
}

class _DetailTaskScreenState extends State<DetailTaskScreen> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text("Detail task"),
      ),
      body: new DetailTask(task: widget.task),
    );
  }
}

class DetailTask extends StatefulWidget {
  final Task task;
  //constructor
  DetailTask({Key? key, required this.task}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return DetailTaskState();
  }
}

class DetailTaskState extends State<DetailTask> {
  Task task = new Task(id: 0, isfinished: false, name: '', todosId: 0);
  bool isLoadedTask = false;

  @override
  Widget build(BuildContext context) {
    if (!isLoadedTask) {
      setState(() {
        this.task = widget.task;
        this.isLoadedTask = true;
      });
    }
    final TextField _txtTaskName = new TextField(
      decoration: InputDecoration(
        hintText: "Enter task's name",
        contentPadding: EdgeInsets.all(10.0),
        border: OutlineInputBorder(borderSide: BorderSide(color: Colors.black)),
      ),
      autocorrect: false,
      controller: TextEditingController(text: this.task.name),
      textAlign: TextAlign.left,
      onChanged: (text) {
        setState(() {
          this.task.name = text;
        });
      },
    );

    final Text _txtFinished =
        Text("Finished:", style: TextStyle(fontSize: 16.0));
    final Checkbox _cbFinished = Checkbox(
        value: this.task.isfinished,
        onChanged: (value) {
          setState(() {
            this.task.isfinished = value!;
          });
        });
    final _btnSave = ElevatedButton(
      child: Text("Save"),
      onPressed: () async {
        Map<dynamic, dynamic> params = Map<dynamic, dynamic>();
        params["id"] = this.task.id;
        params["name"] = this.task.name;
        params["isfinished"] = this.task.isfinished;
        params["todosId"] = this.task.todosId;
        await updateATask(params);
        Navigator.pop(context);
      },
    );
    final _btnDelete = ElevatedButton(
      onPressed: () async {
        await deleteATask(this.task.id);
      },
      child: Text("Delete"),
    );
    final _column = Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        _txtTaskName,
        Container(
          padding: EdgeInsets.only(left: 10.0, right: 10.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              _txtFinished,
              _cbFinished,
            ],
          ),
        ),
        Row(
          children: [Expanded(child: _btnSave),Expanded(child: _btnDelete)],
        )
      ],
    );
    return Container(
      margin: EdgeInsets.all(10.0),
      child: _column,
    );
  }
}

Future<Task> updateATask(Map<dynamic, dynamic> params) async {
  var id = params["id"].toString();
  print(params);
  final resp = await http.put(
    'http://localhost:3000/tasks/${id}',
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
    },
    body: jsonEncode(params),
  );
  if (resp.statusCode == 200) {
    final respBody = await jsonDecode(resp.body);
    return Task.fromJson(respBody);
  } else {
    throw Exception('Failed to update task. Error: ${resp.toString()}');
  }
}

Future<Task> deleteATask(int id) async {
  final resp = await http.delete('http://localhost:3000/tasks/${id}');
  if (resp.statusCode == 200) {
    final respBody = await jsonDecode(resp.body);
    return Task.fromJson(respBody);
  } else {
    throw Exception('Failed to delete task. Error: ${resp.toString()}');
  }
}
